import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post } from '@nestjs/common';

import { CreateVariantFilesDto } from './dto/create-variant-files.dto';
import { UpdateVariantFilesDto } from './dto/update-variant-files.dto';
import { VariantFiles } from './schemas/variant-files.schema';
import { VariantFilesService } from './variant-files.service';

@Controller('variant_files')
export class VariantFilesController {
    constructor(private readonly variantFilesService: VariantFilesService) {}

    @HttpCode(HttpStatus.OK)
    @Get()
    getAllVariantFiles(): Promise<VariantFiles[]> {
      return this.variantFilesService.findAll();
    }

    @HttpCode(HttpStatus.OK)
    @Get(':id')
    getVariantFiles(@Param('id') id: string): Promise<VariantFiles> {
      return this.variantFilesService.findById(id);
    }

    @HttpCode(HttpStatus.CREATED)
    @Post(':variantId/createVariantFiles')
    createVariantFiles(@Param('variantId') variantId: string, @Body() createVariantFiles: CreateVariantFilesDto): Promise<VariantFiles> {
        return this.variantFilesService.createVariantFiles(variantId, createVariantFiles);
    }

    @HttpCode(HttpStatus.OK)
    @Delete(':id')
    removeVariantFiles(@Param('id') id: string): Promise<VariantFiles> {
        return this.variantFilesService.removeVariantFiles(id);
    }

    @HttpCode(HttpStatus.CREATED)
    @Patch(':id')
    updateVariantFiles(@Param('id') id: string, @Body() updateVariant: UpdateVariantFilesDto): Promise<VariantFiles> {
        return this.variantFilesService.updateVariantFiles(id, updateVariant);
    }
}
