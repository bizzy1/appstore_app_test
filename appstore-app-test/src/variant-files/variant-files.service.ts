import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { CreateVariantFilesDto } from './dto/create-variant-files.dto';
import { UpdateVariantFilesDto } from './dto/update-variant-files.dto';
import { VariantFiles, VariantFilesDocument } from './schemas/variant-files.schema';

@Injectable()
export class VariantFilesService {
    constructor(
        @InjectModel(VariantFiles.name) private variantFilesModel: Model<VariantFilesDocument>
    ) {
        
    }

    async findAll(): Promise<VariantFiles[]> {
        return await this.variantFilesModel.find().exec();
    }

    async findById(id: string): Promise<VariantFiles> {
        return await this.variantFilesModel.findById(id);
    }

    async createVariantFiles(variantId: string, createVariantFiles: CreateVariantFilesDto): Promise<VariantFiles> {
        createVariantFiles.variantId = variantId;
        createVariantFiles.createdAt = new Date();
        createVariantFiles.updatedAt = null;
        const createdVariant = new this.variantFilesModel(createVariantFiles);
        return createdVariant.save();
    }

    async updateVariantFiles(id: string, updateVariantFiles: UpdateVariantFilesDto): Promise<VariantFiles> {
        updateVariantFiles.updatedAt = new Date();
        return this.variantFilesModel.findByIdAndUpdate(id, updateVariantFiles);

    }

    async removeVariantFiles(id: string): Promise<VariantFiles> {
        return this.variantFilesModel.findByIdAndRemove(id);
    }
}
