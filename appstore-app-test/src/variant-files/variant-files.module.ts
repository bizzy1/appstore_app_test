import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { VariantFiles, VariantFilesSchema } from './schemas/variant-files.schema';
import { VariantFilesController } from './variant-files.controller';
import { VariantFilesService } from './variant-files.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: VariantFiles.name, schema: VariantFilesSchema }])],
  controllers: [VariantFilesController],
  providers: [VariantFilesService]
})
export class VariantFilesModule {}
