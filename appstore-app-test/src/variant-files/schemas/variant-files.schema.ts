import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type VariantFilesDocument = VariantFiles & Document;

@Schema()
export class VariantFiles {
  _id: string;

  @Prop()
  variantId: string;

  @Prop()
  fileUrl: string;

  @Prop()
  createdAt: Date;

  @Prop()
  updatedAt: Date;

}

export const VariantFilesSchema = SchemaFactory.createForClass(VariantFiles);