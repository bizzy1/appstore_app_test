import { IsNotEmpty, IsString } from "class-validator";


export class CreateVariantFilesDto {

    variantId: string;
  
    @IsNotEmpty()
    @IsString()
    fileUrl: string;

    createdAt: Date;

    updatedAt: null;
    
}