import { IsNotEmpty, IsString } from "class-validator";


export class UpdateVariantFilesDto {

    @IsNotEmpty()
    @IsString()
    fileUrl: string;

    updatedAt: Date;
    
}