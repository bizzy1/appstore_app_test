import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppsModule } from './apps/apps.module';
import { VariantsModule } from './variants/variants.module';
import { VariantFilesModule } from './variant-files/variant-files.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    AppsModule,
    MongooseModule.forRoot(process.env.DATABASE_CONNECT),
    VariantsModule,
    VariantFilesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
