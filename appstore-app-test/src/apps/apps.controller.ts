import { Controller, HttpCode, HttpStatus, Get, Param, Post, Body, Delete, Patch } from '@nestjs/common';

import { App } from './schemas/app.schema';
import { CreateAppDto } from './dto/create-app.dto';
import { UpdateAppDto } from './dto/update-app.dto';
import { AppsService } from './apps.service';

@Controller('apps')
export class AppsController {
    constructor(private readonly appsService: AppsService) {}

    @HttpCode(HttpStatus.OK)
    @Get()
    getAllApps(): Promise<App[]> {
      return this.appsService.findAll();
    }

    @HttpCode(HttpStatus.OK)
    @Get(':id')
    getApp(@Param('id') id: string): Promise<App> {
      return this.appsService.findById(id);
    }

    @HttpCode(HttpStatus.CREATED)
    @Post()
    createApp(@Body() createApp: CreateAppDto): Promise<App> {
        return this.appsService.createApp(createApp);
    }

    @HttpCode(HttpStatus.OK)
    @Delete(':id')
    removeApp(@Param('id') id: string): Promise<App> {
        return this.appsService.removeApp(id);
    }

    @HttpCode(HttpStatus.CREATED)
    @Patch(':id')
    updateApp(@Param('id') id: string, @Body() updateApp: UpdateAppDto): Promise<App> {
        return this.appsService.updateApp(id, updateApp);
    }

}
