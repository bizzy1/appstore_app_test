import { IsNotEmpty, IsString } from "class-validator";


export class UpdateAppDto {

    @IsString()
    @IsNotEmpty()
    readonly name: string;

    updatedAt: Date;
}