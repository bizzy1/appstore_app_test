import { IsNotEmpty, IsString } from "class-validator";

export class CreateAppDto {

    @IsString()
    @IsNotEmpty()
    readonly name: string;

    createdAt: Date;

    updatedAt: null;
}