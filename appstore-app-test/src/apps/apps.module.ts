import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppsController } from './apps.controller';
import { AppsService } from './apps.service';
import { App, AppSchema } from './schemas/app.schema';
import { VariantsService } from 'src/variants/variants.service';
import { Variant, VariantSchema } from 'src/variants/schemas/variant.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: App.name, schema: AppSchema }]),
    MongooseModule.forFeature([{ name: Variant.name, schema: VariantSchema }])
  ],  
  controllers: [AppsController],
  providers: [AppsService, VariantsService]
})
export class AppsModule {}
