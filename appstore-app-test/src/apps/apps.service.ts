import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { App, AppDocument } from './schemas/app.schema';
import { CreateAppDto } from './dto/create-app.dto';
import { UpdateAppDto } from './dto/update-app.dto';

@Injectable()
export class AppsService {
    constructor(
        @InjectModel(App.name) private appModel: Model<AppDocument>
    ) {
        
    }

    async findAll(): Promise<App[]> {
        return await this.appModel.find().exec();
    }

    async findById(id: string): Promise<App> {
        return await this.appModel.findById(id);
    }

    async createApp(createApp: CreateAppDto): Promise<App> {
        createApp.createdAt = new Date();
        createApp.updatedAt = null;
        const createdApp = new this.appModel(createApp);
        return createdApp.save();
    }

    async updateApp(id: string, updateApp: UpdateAppDto): Promise<App> {
        updateApp.updatedAt = new Date();
        return this.appModel.findByIdAndUpdate(id, updateApp);

    }

    async removeApp(id: string): Promise<App> {
        return this.appModel.findByIdAndRemove(id);
    }
}
