import { IsDate, IsNotEmpty, IsNumber, IsString } from "class-validator";


export class UpdateVariantDto {

    @IsString()
    pictureUrl: string;

    @IsString()
    title: string;
  
    @IsString()
    developerName: string;
  
    @IsNumber()
    rating: number;
  
    @IsString()
    age: string;

    @IsString()
    appUrl: string;

    updatedAt: Date;
    
}