import { IsNotEmpty, IsNumber, IsString } from "class-validator";


export class CreateVariantDto {

    appId: string;

    pictureUrl: string;

    @IsString()
    @IsNotEmpty()
    readonly title: string;
  
    @IsString()
    @IsNotEmpty()
    readonly developerName: string;
  
    @IsNumber()
    @IsNotEmpty()
    readonly rating: number;
  
    @IsString()
    @IsNotEmpty()
    readonly age: string;

    @IsString()
    @IsNotEmpty()
    readonly appUrl: string;

    click: number;

    createdAt: Date;

    updatedAt: null;
    
}