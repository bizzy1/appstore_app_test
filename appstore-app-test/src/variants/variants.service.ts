import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Variant, VariantDocument } from './schemas/variant.schema';
import { CreateVariantDto } from './dto/create-variant.dto';
import { UpdateVariantDto } from './dto/update-variant.dto';

@Injectable()
export class VariantsService {
    constructor(
        @InjectModel(Variant.name) private variantModel: Model<VariantDocument>
    ) {
        
    }

    async findAll(): Promise<Variant[]> {
        return await this.variantModel.find().exec();
    }

    async findById(id: string): Promise<Variant> {
        return await this.variantModel.findById(id);
    }

    async createVariant(appId: string, createVariant: CreateVariantDto): Promise<Variant> {
        createVariant.appId = appId;
        createVariant.click = 0;
        createVariant.createdAt = new Date();
        createVariant.updatedAt = null;
        createVariant.pictureUrl = '/mock/picture/Url';
        const createdVariant = new this.variantModel(createVariant);
        return createdVariant.save();
    }

    async updateVariant(id: string, updateVariant: UpdateVariantDto): Promise<Variant> {
        console.log('1 updateVariant', updateVariant)
        updateVariant.updatedAt = new Date();
        console.log('2 updateVariant', updateVariant)
        return this.variantModel.findByIdAndUpdate(id, updateVariant);
    }

    async updateVariantClick(variantId: string): Promise<Variant> {
        return this.variantModel.findByIdAndUpdate(variantId, { $inc: {click: 1} });
    }


    async removeVariant(id: string): Promise<Variant> {
        return this.variantModel.findByIdAndRemove(id);
    }
}

