import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type VariantDocument = Variant & Document;

@Schema()
export class Variant {
  _id: string;

  @Prop()
  appId: string;

  @Prop()
  pictureUrl: string;

  @Prop()
  title: string;

  @Prop()
  developerName: string;

  @Prop()
  rating: number;

  @Prop()
  age: string;

  @Prop()
  appUrl: string;

  @Prop()
  click: number;

  @Prop()
  createdAt: Date;

  @Prop()
  updatedAt: Date;

}

export const VariantSchema = SchemaFactory.createForClass(Variant);