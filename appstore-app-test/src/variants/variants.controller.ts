import { Controller, HttpCode, HttpStatus, Get, Param, Post, Body, Delete, Patch, Put } from '@nestjs/common';

import { VariantsService } from './variants.service';
import { Variant } from './schemas/variant.schema';
import { CreateVariantDto } from './dto/create-variant.dto';
import { UpdateVariantDto } from './dto/update-variant.dto';

@Controller('variants')
export class VariantsController {
    constructor(private readonly variantsService: VariantsService) {}

    @HttpCode(HttpStatus.OK)
    @Get()
    getAllVariants(): Promise<Variant[]> {
      return this.variantsService.findAll();
    }

    @HttpCode(HttpStatus.OK)
    @Get(':id')
    getVariant(@Param('id') id: string): Promise<Variant> {
      return this.variantsService.findById(id);
    }

    @HttpCode(HttpStatus.CREATED)
    @Post(':appId/createVariant')
    createVariant(@Param('appId') appId: string, @Body() createVariant: CreateVariantDto): Promise<Variant> {
        return this.variantsService.createVariant(appId, createVariant);
    }

    @HttpCode(HttpStatus.OK)
    @Delete(':id')
    removeVariant(@Param('id') id: string): Promise<Variant> {
        return this.variantsService.removeVariant(id);
    }

    @HttpCode(HttpStatus.CREATED)
    @Put(':variantId/click')
    updateVariantClick(@Param('variantId') variantId: string): Promise<Variant> {
        return this.variantsService.updateVariantClick(variantId);
    }

    @HttpCode(HttpStatus.CREATED)
    @Patch(':id')
    updateVariant(@Param('id') id: string, @Body() updateVariant: UpdateVariantDto): Promise<Variant> {
        return this.variantsService.updateVariant(id, updateVariant);
    }
}
