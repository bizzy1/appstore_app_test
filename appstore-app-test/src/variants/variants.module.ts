import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { VariantsService } from './variants.service';
import { Variant, VariantSchema } from './schemas/variant.schema';
import { VariantsController } from './variants.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Variant.name, schema: VariantSchema }])
  ],  
  controllers: [VariantsController],
  providers: [VariantsService]
})
export class VariantsModule {}
